#
# To install dependencies on debian:
# apt-get install libc-client2007e libc-client2007e-dev libkrb5-dev libmysqlclient-dev libxml2-dev libcurl4-gnutls-dev pkg-config libmcrypt-dev libxslt1-dev
#

PHP52=5.2.17
PHP53=5.3.29
PHP54=5.4.42
PHP55=5.5.32
PHP56=5.6.18
PHP70=7.0.2

all: php52 php53 php54 php55 php56 php70

php-$(PHP52):
	wget -O php-$(PHP52).tar.bz2 "http://museum.php.net/php5/php-$(PHP52).tar.bz2"
	tar jxf php-$(PHP52).tar.bz2

php-$(PHP53):
	wget -O php-$(PHP53).tar.bz2 "http://pl1.php.net/get/php-$(PHP53).tar.bz2/from/this/mirror"
	tar jxf php-$(PHP53).tar.bz2

php-$(PHP54):
	wget -O php-$(PHP54).tar.bz2 "http://pl1.php.net/get/php-$(PHP54).tar.bz2/from/this/mirror"
	tar jxf php-$(PHP54).tar.bz2

php-$(PHP55):
	wget -O php-$(PHP55).tar.bz2 "http://pl1.php.net/get/php-$(PHP55).tar.bz2/from/this/mirror"
	tar jxf php-$(PHP55).tar.bz2

php-$(PHP56):
	wget -O php-$(PHP56).tar.bz2 "http://pl1.php.net/get/php-$(PHP56).tar.bz2/from/this/mirror"
	tar jxf php-$(PHP56).tar.bz2

php-$(PHP70):
	wget -O php-$(PHP70).tar.bz2 "http://pl1.php.net/get/php-$(PHP70).tar.bz2/from/this/mirror"
	tar jxf php-$(PHP70).tar.bz2

php52: php-$(PHP52)
	mkdir -p /usr/local/kerberos && \
	[[ -e /usr/local/kerberos/lib ]] || ln -s /usr/lib/x86_64-linux-gnu/ /usr/local/kerberos/lib
	cd php-$(PHP52) && \
	[[ -e .patched ]] || patch -p0 < ../php-52-fix-new-libxml.patch && touch .patched && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos=/usr/local/kerberos/ --prefix=/usr/local/php/$(PHP52) && \
	make && \
	make install

php53: php-$(PHP53)
	cd php-$(PHP53) && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos --prefix=/usr/local/php/$(PHP53) && \
	make && \
	make install

php54: php-$(PHP54)
	cd php-$(PHP54) && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos --prefix=/usr/local/php/$(PHP54) && \
	make && \
	make install

php55: php-$(PHP55)
	cd php-$(PHP55) && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos --prefix=/usr/local/php/$(PHP55) && \
	make && \
	make install

php56: php-$(PHP56)
	cd php-$(PHP56) && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos  --prefix=/usr/local/php/$(PHP56) && \
	make && \
	make install

php70: php-$(PHP70)
	cd php-$(PHP70) && \
	./configure --enable-bcmath --enable-calendar --with-curl --with-gettext --with-imap=/usr/lib/dovecot/imap --with-imap-ssl --enable-mbstring --with-mcrypt --with-mhash  --with-mysqli  --enable-soap --with-xmlrpc --with-xsl --enable-zip --with-kerberos  --prefix=/usr/local/php/$(PHP70) && \
	make && \
	make install
