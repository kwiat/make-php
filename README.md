Simple Makefile that helps install multiple PHP versions.
Tested with Debian 6, 7 and 8.

Before start install dependencies:
    
    apt-get install libc-client2007e libc-client2007e-dev libkrb5-dev libmysqlclient-dev libxml2-dev libcurl4-gnutls-dev pkg-config libmcrypt-dev libxslt1-dev

## Examples
Install php 5.3:

    make php53

Install all defined PHP versions:

    make all

Please set desired versions of PHP in the Makefile. make(1) will download sources, extract them, compile and install in /usr/local/php/:

    # ls /usr/local/php/
    5.3.27	5.3.29	5.4.31	5.5.15

Old versions are not uninstalled automatically. To do that you need to simple remove directory:

    rm -rf /usr/local/php/5.3.27/
